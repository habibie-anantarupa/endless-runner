# Endless Runner
Simple endless runner game with only coins and 1 kind of obstacle.

## How to Play
Move between 3 lanes using arrow keys left and right or screen buttons. Collect coins and avoid obstacles. Run as far as you can!\
Press P to pause the game.
