using System.Collections.Generic;
using System.ComponentModel;
using EndlessRunner.Interactables;
using EndlessRunner.ObjectPool;
using UnityEngine;

namespace EndlessRunner.ObjectPool
{
    public abstract class PoolObject<T> : MonoBehaviour, IPoolObject<T> where T : IPoolObject<T>
    {
        protected PoolController<T> poolController;
        private bool canBeUsed;

        public virtual void Init(PoolController<T> poolController)
        {
            this.poolController = poolController;
        }
        
        public abstract void OnReturnToPool();
        
        public virtual bool CanBeUsed()
        {
            return !gameObject.activeInHierarchy && canBeUsed;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void SetObjectCanBeUsed(bool canBeUsed)
        {
            this.canBeUsed = canBeUsed;
        }
    }
}