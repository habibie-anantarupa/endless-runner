using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.ObjectPool
{
    public interface IPoolObject<T>
    {
        GameObject GetGameObject();
        bool CanBeUsed();
        void OnReturnToPool();
        void SetObjectCanBeUsed(bool canBeUsed);
    }
}
