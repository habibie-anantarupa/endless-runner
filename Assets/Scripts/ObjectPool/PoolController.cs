using System;
using System.Collections.Generic;
using EndlessRunner.Interactables;
using UnityEngine;
using UnityEngine.Pool;

namespace EndlessRunner.ObjectPool
{
    public abstract class PoolController<T> : MonoBehaviour where T : IPoolObject<T>
    {
        [SerializeField] private PoolObjectPrefab<T>[] objectPrefabs;

        private int cloneIndex;
        protected readonly List<T> pooledObject = new List<T>();

        protected PoolObject<T> CurrentPrefab { get; private set; }

        private void Awake()
        {
            OnInitiation();
        }

        public virtual void OnInitiation()
        {
            foreach (PoolObjectPrefab<T> poolObjectPrefab in objectPrefabs)
            {
                CurrentPrefab = poolObjectPrefab.prefab;

                for (int i = 0; i < poolObjectPrefab.instantiateCount; i++)
                {
                    T obj = InstantiateObject();
                    obj.SetObjectCanBeUsed(true);
                }
            }
        }

        public virtual T GetPooledObject<T2>() where T2 : T
        {
            foreach (T obj in pooledObject)
            {
                if (obj.GetType() == typeof(T2) && obj.CanBeUsed())
                {
                    obj.SetObjectCanBeUsed(false);
                    return obj;
                }
            }

            return InstantiateObject<T2>();
        }

        public virtual T GetPooledObject()
        {
            foreach (T obj in pooledObject)
            {
                if (obj.CanBeUsed())
                {
                    obj.SetObjectCanBeUsed(false);
                    return obj;
                }
            }

            return InstantiateObject();
        }

        public virtual void ReturnObjectToPool(T obj)
        {
            GameObject gameObj = obj.GetGameObject();
            gameObj.SetActive(false);
            gameObj.transform.SetParent(transform);
            gameObj.transform.position = Vector3.zero;
            gameObj.transform.localPosition = Vector3.zero;
            obj.OnReturnToPool();
            obj.SetObjectCanBeUsed(true);
        }

        protected virtual void OnInstantiateObject(T obj)
        {
            cloneIndex++;
        }

        private T InstantiateObject()
        {
            if (CurrentPrefab == null || CurrentPrefab is not T)
            {
                // foreach (PoolObjectPrefab<T> poolObjectPrefab in objectPrefabs)
                // {
                //     if (poolObjectPrefab.prefab.GetType() == typeof(T))
                //     {
                //         CurrentPrefab = poolObjectPrefab.prefab;
                //         break;
                //     }
                // }
                CurrentPrefab = FindPrefabOfType<T>();
            }

            T obj = default;

            if (CurrentPrefab != null)
            {
                obj = Instantiate(CurrentPrefab, transform).GetComponent<T>();
                GameObject gameObj = obj.GetGameObject();
                gameObj.SetActive(false);
                gameObj.name += cloneIndex.ToString();
                pooledObject.Add(obj);

                OnInstantiateObject(obj);
            }
            else
            {
                Debug.LogError($"Type {typeof(T)} has no prefab!, returning default");
            }

            return obj;
        }

        private T InstantiateObject<T2>() where T2 : T
        {
            if (CurrentPrefab == null || CurrentPrefab is not T || CurrentPrefab is not T2)
            {
                // foreach (PoolObjectPrefab<T> poolObjectPrefab in objectPrefabs)
                // {
                //     if (poolObjectPrefab.prefab.GetType() == typeof(T2))
                //     {
                //         CurrentPrefab = poolObjectPrefab.prefab;
                //         break;
                //     }
                // }
                CurrentPrefab = FindPrefabOfType<T2>();
            }

            T obj = default;

            if (CurrentPrefab != null)
            {
                obj = Instantiate(CurrentPrefab, transform).GetComponent<T>();
                GameObject gameObj = obj.GetGameObject();
                gameObj.SetActive(false);
                gameObj.name += cloneIndex.ToString();
                pooledObject.Add(obj);

                OnInstantiateObject(obj);
            }
            else
            {
                Debug.LogError($"Type {typeof(T2)} has no prefab!, returning default");
            }

            return obj;
        }

        private PoolObject<T> FindPrefabOfType<T2>()
        {
            foreach (PoolObjectPrefab<T> poolObjectPrefab in objectPrefabs)
            {
                if (poolObjectPrefab.prefab.GetType() == typeof(T2))
                {
                    return poolObjectPrefab.prefab;
                }
            }

            return null;
        }

        public virtual void ReturnAllPoolObject()
        {
            foreach (T poolObject in pooledObject)
            {
                ReturnObjectToPool(poolObject);
            }
        }
    }

    [Serializable]
    public class PoolObjectPrefab<T> where T : IPoolObject<T>
    {
        public PoolObject<T> prefab;
        public int instantiateCount;
    }
}