using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.UI
{
    public class PopupController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        public void Show()
        {
            OnShow();
        }

        public void Hide()
        {
            OnHide();
        }

        protected virtual void OnShow()
        {
            if (canvas)
            {
                canvas.enabled = true;
            }
            else
            {
                gameObject.SetActive(true);
            }

            if (canvas.gameObject != gameObject)
            {
                gameObject.SetActive(true);
            }
        }

        protected virtual void OnHide()
        {
            if (canvas)
            {
                canvas.enabled = false;
            }
            else
            {
                gameObject.SetActive(false);
            }

            if (canvas.gameObject != gameObject)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
