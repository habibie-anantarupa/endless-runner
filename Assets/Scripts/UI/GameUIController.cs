using System;
using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EndlessRunner.UI
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TextMeshProUGUI coinText, distanceText;
        [SerializeField] private Image[] healthIndicators;

        private void Awake() 
        {
            gameManager.CoinChanged += OnCoinChanged;    
            gameManager.HealthChanged += OnHealthChanged;    
            gameManager.DistanceChanged += OnDistanceChanged;
        }

        private void OnDestroy() 
        {
            gameManager.CoinChanged -= OnCoinChanged;    
            gameManager.HealthChanged -= OnHealthChanged;    
            gameManager.DistanceChanged -= OnDistanceChanged;
        }

        private void OnCoinChanged(int coin)
        {
            coinText.text = coin.ToString();
        }

        private void OnHealthChanged(int health)
        {
            for (int i = healthIndicators.Length - 1; i >= 0; i--)
            {
                healthIndicators[i].gameObject.SetActive(i < health);
            }
        }

        private void OnDistanceChanged(float distance)
        {
            int distanceRounded = Mathf.RoundToInt(distance);
            distanceText.text = distanceRounded.ToString();
        }
    }
}
