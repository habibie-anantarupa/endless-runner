using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace EndlessRunner.UI
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button playButton, quitButton;

        public event Action PlayPressed;
        private void Awake() 
        {
            playButton.onClick.AddListener(Play);
            quitButton.onClick.AddListener(Quit);
        }

        private void OnDestroy() 
        {
            playButton.onClick.RemoveAllListeners();
            quitButton.onClick.RemoveAllListeners();
        }

        protected override void OnShow()
        {
            base.OnShow();
        }

        private void OnPlayModePressed()
        {
            Hide();
            PlayPressed?.Invoke();
        }

        private void Play()
        {
            OnPlayModePressed();
        }

        private void Quit()
        {
            EditorApplication.ExitPlaymode();
        }
    }
}
