using System;
using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Controllers;
using EndlessRunner.Terrain;
using EndlessRunner.Interactables;
using EndlessRunner.UI;
using UnityEngine;

namespace EndlessRunner.Game
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerController playerController;
        [SerializeField] private TerrainPoolController terrainPoolController;
        [SerializeField] private StartMenuController startMenuController;
        [SerializeField] private PausePopupController pausePopupController;
        [SerializeField] private GameOverPopupController gameOverPopupController;

        private const float TerrainForwardDistance = 8f;
        private const float TerrainSpawnDistance = 10f;

        private int lastSpawnedIndex;
        private int health, coin;
        private float distance;
        private bool isPaused, isInGame;

        public event Action<int> HealthChanged, CoinChanged;
        public event Action<float> DistanceChanged;
        
        public int Health 
        {
            get => health; 
            private set
            {
                health = value;
                HealthChanged?.Invoke(health);
                if (CheckForGameOver())
                {
                    GameOver();
                }
            }
        }

        public int Coin
        {
            get => coin;
            private set
            {
                coin = value;
                CoinChanged?.Invoke(coin);
            }
        }

        public float Distance
        {
            get => distance;
            private set
            {
                distance = value;
                DistanceChanged?.Invoke(distance);
            }
        }

        private void Awake()
        {
            startMenuController.PlayPressed += OnPlayPressed;

            playerController.HitCoin += OnHitCoin;
            playerController.HitObstacle += OnHitObstacle;

            pausePopupController.ContinuePressed += OnContinuePressed;
            pausePopupController.RestartPressed += OnRestartPressed;
            pausePopupController.QuitPressed += OnQuitPressed;

            gameOverPopupController.PlayAgainPressed += OnRestartPressed;
            gameOverPopupController.QuitPressed += OnQuitPressed;

            Init();
        }

        private void Update() 
        {
            if (isInGame &&!isPaused && Input.GetKeyDown(KeyCode.P))
            {
                Pause();
            }

            if (playerController.CanMove)
            {
                Distance += Time.deltaTime * playerController.MoveSpeed;
            }
        }

        private void OnDestroy() 
        {
            startMenuController.PlayPressed -= OnPlayPressed;

            playerController.HitCoin -= OnHitCoin;
            playerController.HitObstacle -= OnHitObstacle;

            pausePopupController.ContinuePressed -= OnContinuePressed;
            pausePopupController.RestartPressed -= OnRestartPressed;
            pausePopupController.QuitPressed -= OnQuitPressed;

            gameOverPopupController.PlayAgainPressed -= OnRestartPressed;
            gameOverPopupController.QuitPressed -= OnQuitPressed;
        }

        private void Init()
        {
            Reset();
        }

        private void OnPlayPressed()
        {
            StartGame();
        }

        private void StartGame()
        {
            for (int i = 0; i < 5; i++)
            {
                SpawnTerrainOnIndex();
            }

            pausePopupController.Hide();
            gameOverPopupController.Hide();
            startMenuController.Hide();
            isInGame = true;
            playerController.Activate();
        }

        private void OnTerrainReturnDistancePassed(TerrainPieceController terrainPieceController)
        {
            SpawnTerrainOnIndex();
            terrainPieceController.Deactivate();
        }

        private void SpawnTerrainOnIndex()
        {
            TerrainPieceController terrain = terrainPoolController.GetPooledObject();
            terrain.transform.position = Vector3.forward * TerrainSpawnDistance * lastSpawnedIndex;

            terrain.Spawn(terrain.transform.position.z + TerrainForwardDistance, playerController, OnTerrainReturnDistancePassed);
            terrain.Activate();
            lastSpawnedIndex++;
        }

        private void OnHitCoin(Coin coin)
        {
            coin.Unassign();
            terrainPoolController.InteractablePoolController.ReturnObjectToPool(coin);
            Coin++;
        }

        private void OnHitObstacle(Obstacle obstacle)
        {
            obstacle.Unassign();
            terrainPoolController.InteractablePoolController.ReturnObjectToPool(obstacle);
            Health--;
        }

        private void Pause()
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                pausePopupController.Show();
                playerController.Deactivate();
            }
            else
            {
                pausePopupController.Hide();
                playerController.Activate();
            }
        }

        private bool CheckForGameOver()
        {
            return Health <= 0;
        }

        private void GameOver()
        {
            playerController.Deactivate();
            gameOverPopupController.Show();
        }

        private void Reset()
        {
            terrainPoolController.ReturnAllPoolObject();

            Health = 3;
            Coin = 0;
            distance = 0f;
            lastSpawnedIndex = 0;
            isPaused = false;

            playerController.Reset();
        }
        
        private void OnRestartPressed()
        {
            Reset();
            StartGame();
        }

        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnQuitPressed()
        {
            startMenuController.Show();
            gameOverPopupController.Hide();
            Reset();
            isInGame = false;
        }
    }
}
