using EndlessRunner.ObjectPool;
using EndlessRunner.Interactables;
using System.Collections.Generic;

namespace EndlessRunner.ObjectPool
{
    public class InteractablePoolController : PoolController<Interactable>
    {
        public Coin GetPooledCoin()
        {
            return (Coin) GetPooledObject<Coin>();
        }

        public Obstacle GetPooledObstacle()
        {
            return (Obstacle) GetPooledObject<Obstacle>();
        }
    }
}
