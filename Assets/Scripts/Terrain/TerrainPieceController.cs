using System;
using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Controllers;
using EndlessRunner.Terrain;
using EndlessRunner.ObjectPool;
using UnityEngine;
using EndlessRunner.Interactables;
using UnityEngine.Pool;
using System.Linq;

public class TerrainPieceController : PoolObject<TerrainPieceController>, IInteractableContainer
{
    private const float TerrainLaneDistance = 3.33f; // 1 / 3 = 3.33f
    private const int InteractableSpawnChance = 50;
    private const int CoinSpawnChance = 30;
    private const int ObstacleSpawnChance = 10;

    private float triggerReturnDistance;
    private bool canCalculate;
    private IMoveable moveableObject;
    private TerrainPoolController terrainPoolController;

    private Vector3[] spawnPoints = new Vector3[9];
    private readonly List<Interactable> interactables = new List<Interactable>();

    public event Action<TerrainPieceController> ReturnDistancePassed;

    public void Init(TerrainPoolController poolController)
    {
        this.terrainPoolController = poolController;
        int index = 0;
        for (int row = -1; row <= 1; row++)
        {
            for (int col = -1; col <= 1; col++)
            {
                Vector3 position = TerrainLaneDistance * (Vector3.forward * row + Vector3.right * col);
                spawnPoints[index] = position;
                index++;
            }
        }
    }

    public void Spawn(float triggerReturnDistance, IMoveable moveableObject, Action<TerrainPieceController> onReturnDistancePassed)
    {
        this.moveableObject = moveableObject;
        this.triggerReturnDistance = triggerReturnDistance;
        ReturnDistancePassed += onReturnDistancePassed;

        SpawnInteractables();
    }

    private void SpawnInteractables()
    {
        foreach (Transform child in transform)
        {
            if (child.TryGetComponent<Interactable>(out Interactable interactable))
            {
                interactable.Unassign();
                terrainPoolController.InteractablePoolController.ReturnObjectToPool(interactable);
            }
        }

        foreach (Vector3 position in spawnPoints)
        {
            int spawnChance = UnityEngine.Random.Range(0, 101);
            if (spawnChance > InteractableSpawnChance)
            {
                int interactableChance = UnityEngine.Random.Range(0, 101);
                Interactable interactable = null;
                Vector3 spawnPosition = position;

                if (interactableChance >= 100 - ObstacleSpawnChance)
                {
                    interactable = terrainPoolController.InteractablePoolController.GetPooledObject<Obstacle>();
                }
                else if (interactableChance >= 100 - CoinSpawnChance)
                {
                    interactable = terrainPoolController.InteractablePoolController.GetPooledObject<Coin>();
                    spawnPosition.y = 1f;
                }

                if (interactable != null)
                {
                    interactable.transform.SetParent(transform);
                    interactable.gameObject.transform.localPosition = spawnPosition;
                    interactable.gameObject.SetActive(true);

                    interactable.AssignContainer(this);
                }
            }
        }
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        canCalculate = true;
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
        canCalculate = false;
        moveableObject = null;
        ReturnDistancePassed = null;

        for (int i = 0; i < interactables.Count; i++)
        {
            Interactable item = interactables[i];
            item.Unassign();
            terrainPoolController.InteractablePoolController.ReturnObjectToPool(item);
        }

        terrainPoolController.ReturnObjectToPool(this);
    }

    private void OnReturnDistancePassed()
    {
        ReturnDistancePassed?.Invoke(this);
    }

    private void Update()
    {
        if (!gameObject.activeInHierarchy || !canCalculate) return;

        if (moveableObject != null)
        {
            if (CheckHasPassedReturnDistance())
            {
                OnReturnDistancePassed();
            }
        }
    }

    private bool CheckHasPassedReturnDistance()
    {
        return moveableObject.GetPosition().z >= triggerReturnDistance;
    }

    public override void OnReturnToPool()
    {

    }

    public List<IInteractable> GetInteractables()
    {
        return interactables.ToList<IInteractable>();
    }

    public void AssignInteractable(IInteractable interactable, bool assign = true)
    {
        Interactable interactableConverted = (Interactable)interactable;
        if (assign)
        {
            if (!interactables.Contains(interactableConverted))
            {
                interactables.Add(interactableConverted);
            }
        }
        else
        {
            if (interactables.Contains(interactableConverted))
            {
                interactables.Remove(interactableConverted);
            }
        }
    }
}
