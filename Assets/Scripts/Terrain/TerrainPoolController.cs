using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Interactables;
using EndlessRunner.ObjectPool;
using UnityEngine;

namespace EndlessRunner.Terrain
{
    public class TerrainPoolController : PoolController<TerrainPieceController>
    {
        [SerializeField] private InteractablePoolController interactablePoolController;

        public InteractablePoolController InteractablePoolController { get => interactablePoolController; private set => interactablePoolController = value; }

        protected override void OnInstantiateObject(TerrainPieceController obj)
        {
            base.OnInstantiateObject(obj);
            obj.Init(this);
        }

        public override void ReturnObjectToPool(TerrainPieceController terrainPieceController)
        {
            base.ReturnObjectToPool(terrainPieceController);
        }

        public override void ReturnAllPoolObject()
        {
            foreach (TerrainPieceController terrainPieceController in pooledObject)
            {
                terrainPieceController.Deactivate();
            }
        }
    }
}
