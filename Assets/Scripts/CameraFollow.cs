using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.Game
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform objectToFollow;
        [SerializeField] private Vector3 offset;
        
        private void Awake() 
        {
            offset = transform.position;
        }

        private void LateUpdate() 
        {
            transform.position = objectToFollow.position + offset;
        }
    }
}
