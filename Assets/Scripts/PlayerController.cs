using System;
using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Game;
using EndlessRunner.Interactables;
using UnityEngine;

namespace EndlessRunner.Controllers
{
    public class PlayerController : MonoBehaviour, IMoveable, IInteractor
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private float horizontalMoveSpeed;
        [SerializeField] private float horizontalMoveDistance;
        [SerializeField] private InputManager inputManager;
        [SerializeField] int[] lanePositions;
        [SerializeField] private Transform model;
        
        private const float HorizontalMoveDelay = 0.1f;

        private int currentLane;
        private float horizontalMoveInput;
        private bool isMovingHorizontally;
        private bool canMove;
        private Rigidbody rb;

        private Vector3[] lanePositionVectors = new Vector3[3];
        private Coroutine moveCoroutine;
        private YieldInstruction horizontalMoveDelayYieldInstruction;

        public event Action<Coin> HitCoin;
        public event Action<Obstacle> HitObstacle;

        public float MoveSpeed => moveSpeed;
        public bool CanMove => canMove;

        private void Awake() 
        {
            rb = GetComponent<Rigidbody>();
            for (int i = 0; i < lanePositions.Length; i++)
            {
                lanePositionVectors[i] = model.transform.localPosition + model.transform.right * horizontalMoveDistance * lanePositions[i];
            }

            horizontalMoveDelayYieldInstruction = new WaitForSeconds(HorizontalMoveDelay);
            Reset();
        }

        private void Update() 
        {
            if (!canMove) return;

            rb.velocity = transform.forward * moveSpeed;

            if (isMovingHorizontally) return;

            horizontalMoveInput = inputManager.HorizontalInput;

            if (horizontalMoveInput != 0f)
            {
                // currentLane = Mathf.Clamp(currentLane + (horizontalMoveInput > 0f ? 1 : -1), 0, lanePositions.Length - 1);
                currentLane += horizontalMoveInput > 0f ? 1 : -1;

                if (currentLane > lanePositions.Length - 1 || currentLane < 0)
                {
                    currentLane = Mathf.Clamp(currentLane, 0, lanePositions.Length - 1);
                    return;
                }
                else
                {
                    HorizontalMove();
                }
            }
        }
        
        private void HorizontalMove()
        {
            isMovingHorizontally = true;
            if (moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }
            moveCoroutine = StartCoroutine(HorizontalMoveCoroutine());
        }

        private IEnumerator HorizontalMoveCoroutine()
        {
            Vector3 startPosition = model.localPosition;
            // Vector3 movePosition = startPosition + Vector3.right * horizontalMoveDistance * horizontalMoveInput;
            Vector3 movePosition = lanePositionVectors[currentLane];

            for (float t = 0; t < 1; t += Time.deltaTime * moveSpeed)
            {
                model.localPosition = Vector3.Lerp(startPosition, movePosition, t);
                yield return null;
            }

            model.localPosition = movePosition;

            yield return horizontalMoveDelayYieldInstruction;

            isMovingHorizontally = false;
        }

        public void Activate()
        {
            canMove = true;
        }

        public void Deactivate()
        {
            canMove = false;
            rb.velocity = Vector3.zero;
        }

        public void Reset()
        {
            Deactivate();
            transform.position = Vector3.zero;
            model.transform.position = Vector3.zero;

            currentLane = 1;

            // Activate();
        }

        public void OnHitCoin(Coin coin)
        {
            HitCoin?.Invoke(coin);
        }

        public void OnHitObstacle(Obstacle obstacle)
        {
            HitObstacle?.Invoke(obstacle);
        }

        private void OnTriggerEnter(Collider other) 
        {
            if (other.TryGetComponent<IInteractable>(out IInteractable interactable))
            {
                interactable.Interact(this);
            }
        }

        private void OnCollisionEnter(Collision other) 
        {
            if (other.collider.TryGetComponent<IInteractable>(out IInteractable interactable))
            {
                interactable.Interact(this);
            }
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void InteractWith(IInteractable interactable)
        {
            if (interactable is Coin)
            {
                OnHitCoin(interactable as Coin);
            }
            else if (interactable is Obstacle)
            {
                OnHitObstacle(interactable as Obstacle);
            }
        }
    }
}
