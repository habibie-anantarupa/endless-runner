using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EndlessRunner.Game
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField] private string horizontalInputName;
        [SerializeField] private Button leftButton, rightButton;
        
        public float HorizontalInput {get; private set;}

        private void Awake() 
        {
            leftButton.onClick.AddListener(LeftButtonPressed);
            rightButton.onClick.AddListener(RightButtonPressed);
        }

        private void OnDestroy() 
        {
            leftButton.onClick.RemoveAllListeners();
            rightButton.onClick.RemoveAllListeners();
        }

        private void Update() 
        {
            HorizontalInput = Input.GetAxisRaw(horizontalInputName);
        }

        private void LeftButtonPressed()
        {
            HorizontalInput = -1f;
        }

        private void RightButtonPressed()
        {
            HorizontalInput = 1f;
        }
    }
}
