using EndlessRunner.Controllers;
using EndlessRunner.Interactables;
using EndlessRunner.ObjectPool;
using UnityEngine;

public abstract class Interactable : PoolObject<Interactable>, IInteractable
{
    private IInteractableContainer container;
    
    public abstract void Interact(IInteractor interactor);

    public void AssignContainer(IInteractableContainer interactableContainer)
    {
        container = interactableContainer;
        container.AssignInteractable(this);
    }
    

    public void Unassign()
    {
        container?.AssignInteractable(this, false);

        container = null;
    }
}