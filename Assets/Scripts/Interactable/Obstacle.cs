using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Controllers;
using EndlessRunner.Interactables;
using EndlessRunner.ObjectPool;
using UnityEngine;

namespace EndlessRunner.Interactables
{
    public class Obstacle : Interactable
    {
        public override void Interact(IInteractor interactor)
        {
            interactor.InteractWith(this as Interactable);
        }

        public override void OnReturnToPool()
        {
            
        }
    }
}
