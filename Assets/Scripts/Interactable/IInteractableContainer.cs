using System.Collections.Generic;
using UnityEngine;
using EndlessRunner.Controllers;

namespace EndlessRunner.Interactables
{
    public interface IInteractableContainer
    {
        List<IInteractable> GetInteractables();
        void AssignInteractable(IInteractable interactable, bool assign = true);
    }
}
