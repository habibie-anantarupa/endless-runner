using System.Collections.Generic;
using UnityEngine;
using EndlessRunner.Controllers;

namespace EndlessRunner.Interactables
{
    public interface IInteractable
    {
        void Interact(IInteractor interactor);
        void AssignContainer(IInteractableContainer interactableContainer);
        void Unassign();
    }
}
