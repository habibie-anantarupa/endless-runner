using EndlessRunner.Controllers;
using EndlessRunner.Interactables;
using EndlessRunner.ObjectPool;
using UnityEngine;

namespace EndlessRunner.Interactables
{
    public class Coin : Interactable
    {
        public override void Interact(IInteractor interactor)
        {
            interactor.InteractWith(this as IInteractable);
        }

        public override void OnReturnToPool()
        {
            
        }
    }
}
