using EndlessRunner.Interactables;
using UnityEngine;

namespace EndlessRunner.Interactables
{
    public interface IInteractor
    {
        void InteractWith(IInteractable interactable);
    }
}
