
using UnityEngine;

public interface IMoveable
{
    Vector3 GetPosition();
}
